﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace Demo1
{
    class Student : IComparer
    {
       
        
        public string Id { get; set; }
        public string Name { get; set; }
        public string Age { get; set; }

        public int Compare(object x, object y)
        {
            String strx = x.ToString();
            String stry = y.ToString();
            return strx.CompareTo(stry);
        }
        public Student(string Id,string Name,string Age) {
            this.Id = Id;
            this.Name = Name;
            this.Age = Age;
        }
        public override string ToString()
        {
            string info = "学号:" + Id + "姓名:" + Name + "年龄:" + Age;
            return info;
        }

    }
}



