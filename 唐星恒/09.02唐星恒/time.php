<?php
//1. 获取今日开始的时间戳。
date_default_timezone_set("PRC");
$time = strtotime("2021-09-02 00:00:00");  // 将指定日期转成时间戳
echo "<br />";
echo date("Y-m-d H:i:s",$time) . " ";
echo  $time, PHP_EOL;
//2. 获取本周开始的时间戳。
$time2 = strtotime("last monday");  // 将指定日期转成时间戳
echo "<br />";
echo date("Y-m-d H:i:s",$time2) . " ";
echo  $time2, PHP_EOL;
//3. 获取本月开始的时间戳。
$time3 = strtotime("1 September 2021");  // 将指定日期转成时间戳
echo "<br />";
echo date("Y-m-d H:i:s",$time3) . " ";
echo  $time3, PHP_EOL;

