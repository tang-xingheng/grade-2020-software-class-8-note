﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace Demo1
{
    class Id : IComparer
    {
        public int Compare(object x, object y)
        {
            String strx = x.ToString();
            String stry = y.ToString();
            return strx.CompareTo(stry);
        }
    }
}
