create table AccountInfo(
AccountId int identity (1,1) not null ,--账户编号
AccountCode varchar(20) not null ,--身份证号码
AccountPhont varchar (20),--电话号码
RealName varchar (20) not null,--真实姓名
);
alter table AccountInfo add OpenTime smalldatetime not null;
alter table AccountInfo add constraint PK_AccountInfo_AccountId primary key(AccountId);
alter table AccountInfo add constraint UNQ_AccountInfo_AccountCode unique (AccountCode);
alter table AccountInfo add constraint DF_AccountInfo_OpenTime default 'getadte()'for OpenTime;
alter table AccountInfo alter column AccountPhont varchar(20) not null;
create table BankCard (
CardNo varchar(30) primary key,
AccountId int not null,
CardPwd varchar (30) not null,
CardBalance money not null,
CardState tinyint not null,
CardTime varchar(30) not null,
);
alter table BankCard add constraint DF_BankCard_CardBalance default'0.00'for CardBalance;
alter table BankCard add constraint DF_BankCard_CardState  default'1'for CardState ;
alter table BankCard add constraint FK_BankCard_AccountId foreign key (AccountId)references AccountInfo(AccountId);
alter table BankCard alter column CardTime smalldatetime not null;
alter table BankCard add constraint DF_BankCard_CardTime  default (getdate())for CardTime;
alter table BankCard add constraint FK_BankCard_CardNo foreign key (CardNo)references BankCard(CardNo);
create table CardExchange (
ExchageId int identity primary key ,
CardNo varchar(30) not null,
MoneyInBank money not null,
MoneyOutBank money not null,
ExhangeTime smalldatetime not null,
);
alter table CardExchange add constraint CK_CardExchange_MoneyInBank check (MoneyInBank>=0);
alter table CardExchange add constraint CK_CardExchange_MoneyOutBank check(MoneyOutBank>=0);