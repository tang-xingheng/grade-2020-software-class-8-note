create table stuinfo(
stuNO nvarchar(20) primary key,
stuName nvarchar(20) not null,
stuAge int not null,
stuAddress nvarchar(80) not null,
stuSeat int not null,
stuSex tinyint not null,--0男1女2保密
);
create table stuexam(
examNO int not null ,
stuNO nvarchar(20) ,
wittenExam float not null,
labExam float not null,
);
alter table stuexam add constraint FK_stuexam_stuNO foreign key (stuNO)
	references stuinfo(stuNO);

select * from stuinfo;
insert into stuinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,stuSex) 
	values ('s2501','张秋利','20','美国硅谷','1','1')
	,('s2502','李斯文','18','湖北武汉','2','0')
	,('s2503','马文才','22','湖南长沙','3','1')
	,('s2504','欧阳俊雄','21','湖北武汉','4','0')
	,('s2505','梅超风','20','湖北武汉','5','1')
	,('s2506','陈璇风','19','美国硅谷','6','1')
	,('s2507','陈风','20','美国硅谷','7','0');
insert into stuexam(examNO,stuNO,wittenExam,labExam)
	values('1','s2501','50','70')
	,('2','s2502','60','65')
	,('3','s2503','86','85')
	,('4','s2504','40','80')
	,('5','s2505','70','90')
	,('6','s2506','85','90');
select * from stuinfo;
select * from stuexam;
select   stuNO as 学号,stuName as 学生姓名,stuAge as 学生年龄,stuAddress as  学生住址,stuSeat as 学生座号,stuSex as 学生性别  from stuinfo;
select  stuName, stuAge,stuAddress  from stuinfo;
--.查询学生分数表（stuexam）中的学号，笔试和机试三列的信息，并为这三列取中文名字注意：要用三种方法
select stuNO as 学号, 笔试分数=wittenExam, labExam 机试分数 from stuexam;

--查询学生信息表（stuInfo）中的学号，姓名，地址，以及将：姓名+:+手机号 组成新列 “邮箱”
select stuNO,stuName,stuAddress ,stuName+ '@'+ stuAddress '邮箱' from stuinfo;
--.查询学生分数表（stuexam）中的学生的学号，笔试，机试以及总分这四列的信息
select examNO, wittenExam,labExam ,(wittenExam+labExam) '总分' from stuexam
